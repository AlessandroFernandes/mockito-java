package br.com.caelum.leilao.repositories;

import java.util.List;

import br.com.caelum.leilao.dominio.Leilao;

public interface LeilaoDaoRepository {

	public void salva(Leilao leilao);
	public void atualiza(Leilao leilao);
	public List<Leilao> encerrados();
	public List<Leilao> correntes();
}
