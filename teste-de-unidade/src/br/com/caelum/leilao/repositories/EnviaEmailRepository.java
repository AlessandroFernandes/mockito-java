package br.com.caelum.leilao.repositories;

import br.com.caelum.leilao.dominio.Leilao;

public interface EnviaEmailRepository {

	void envia(Leilao leilao);
}
