package br.com.caelum.leilao.repositories;

import br.com.caelum.leilao.dominio.Pagamento;

public interface PagamentoRepository {

	public void salvar(Pagamento pagamento);
}
