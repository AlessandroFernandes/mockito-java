package br.com.caelum.leilao.servico;

import java.util.Calendar;
import java.util.List;

import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Pagamento;
import br.com.caelum.leilao.repositories.LeilaoDaoRepository;
import br.com.caelum.leilao.repositories.PagamentoRepository;
import br.com.caelum.leilao.repositories.RelogioRepository;

public class GeradorDePagamento {
	
	private final LeilaoDaoRepository leilao;
	private final PagamentoRepository pagamentoRepository;
	private final Avaliador avaliador;
	private final RelogioRepository relogio;
	
	public GeradorDePagamento(LeilaoDaoRepository _leilao, PagamentoRepository _pagamento, Avaliador _avaliador, RelogioRepository _relogio)
	{
		this.leilao = _leilao;
		this.pagamentoRepository = _pagamento;
		this.avaliador = _avaliador;
		this.relogio = _relogio;
	}
	
	public GeradorDePagamento(LeilaoDaoRepository leilao, PagamentoRepository pagamento, Avaliador avaliador)
	{
		this(leilao, pagamento, avaliador, new Relogio());
	}
	
	public void gera()
	{
		List<Leilao> leiloes = leilao.encerrados();

		for(Leilao leilao: leiloes)
		{
			avaliador.avalia(leilao);
			
			Pagamento pagamento = new Pagamento(avaliador.getMaiorLance(), diaDaSemana());
			pagamentoRepository.salvar(pagamento);
		}
	}
	
	public Calendar diaDaSemana()
	{
		Calendar horaSistema = relogio.horaSistema();
		int diaSemana = horaSistema.get(Calendar.DAY_OF_WEEK);
		
		if(diaSemana == horaSistema.SATURDAY) {
			horaSistema.add(Calendar.DAY_OF_WEEK, 2);
		} 
		else if(diaSemana == horaSistema.SUNDAY)
		{
			horaSistema.add(Calendar.DAY_OF_WEEK, 1);
		}
		
		return horaSistema;
	}

}
