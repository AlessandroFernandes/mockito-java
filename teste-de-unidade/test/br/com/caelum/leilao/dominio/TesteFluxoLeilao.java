package br.com.caelum.leilao.dominio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.infra.dao.LeilaoDao;
import br.com.caelum.leilao.repositories.EnviaEmailRepository;
import br.com.caelum.leilao.repositories.LeilaoDaoRepository;
import br.com.caelum.leilao.repositories.PagamentoRepository;
import br.com.caelum.leilao.repositories.RelogioRepository;
import br.com.caelum.leilao.servico.Avaliador;
import br.com.caelum.leilao.servico.EncerradorDeLeilao;
import br.com.caelum.leilao.servico.GeradorDePagamento;

public class TesteFluxoLeilao{

	@Test
	public void estaEncerrandoLeilao()
	{
		Calendar data = Calendar.getInstance();
		data.set(2020, 5, 17);
		
		Leilao l1 = new CriadorDeLeilao().para("New Car").naData(data).constroi();
		Leilao l2 = new CriadorDeLeilao().para("Playstation 5").naData(data).constroi();	
		List<Leilao> leiloes = Arrays.asList(l1, l2);
		
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		when(leilao.correntes()).thenReturn(leiloes);
		
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		assertEquals(2, leilao.correntes().size());
		assertEquals(2, encerrador.getTotalEncerrados());
		assertTrue(l1.isEncerrado());
		assertTrue(l2.isEncerrado());
		
	}

	@Test
	public void naoDeveEncerradorLeilao()
	{
		Calendar data = Calendar.getInstance();
		data.add(Calendar.DAY_OF_MONTH, -1);
		
		Leilao l1 = new CriadorDeLeilao().para("New Car").naData(data).constroi();
		Leilao l2 = new CriadorDeLeilao().para("Playstation 5").naData(data).constroi();	
		List<Leilao> leiloes = Arrays.asList(l1, l2);
		
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		when(leilao.correntes()).thenReturn(leiloes);
		
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		assertEquals(2, leilao.correntes().size());
		assertEquals(0, encerrador.getTotalEncerrados());
		assertFalse(l1.isEncerrado());
		assertFalse(l2.isEncerrado());
	}
	
	@Test
	public void listaVaziaDeEncerramento()
	{
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);

		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		when(leilao.correntes()).thenReturn(new ArrayList<Leilao>());
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		assertEquals(0, encerrador.getTotalEncerrados());
	}
	
	@Test
	public void executaAtualizacaoUmaVez()
	{
		Calendar data = Calendar.getInstance();
		data.set(2010, 1, 10);
		
		Leilao l1 = new CriadorDeLeilao().para("New Car").naData(data).constroi();
		List<Leilao> leiloes = Arrays.asList(l1);
		
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		when(leilao.correntes()).thenReturn(leiloes);
		
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);

		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		verify(leilao, times(1)).atualiza(l1);
	}
	
	@Test
	public void naoExecutaAtualizacao()
	{
		Calendar data = Calendar.getInstance();
		data.add(Calendar.DAY_OF_MONTH, -1);
		
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);

		Leilao l1 = new CriadorDeLeilao().para("New Car").naData(data).constroi();
		List<Leilao> leiloes = Arrays.asList(l1);
		
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		when(leilao.correntes()).thenReturn(leiloes);
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		verify(leilao, never()).atualiza(l1);
	}
	
	@Test
	public void ordemDeExecucaoEnvioDeEmail()
	{
		Calendar data = Calendar.getInstance();
		data.set(2020,5,10);
		
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);

		Leilao l1 = new CriadorDeLeilao().para("New Car").naData(data).constroi();
		List<Leilao> leiloes = Arrays.asList(l1);
		
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		when(leilao.correntes()).thenReturn(leiloes);
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		InOrder inOrder = inOrder(leilao, carteiroFake);
		inOrder.verify(leilao, times(1)).atualiza(l1);
		inOrder.verify(carteiroFake, times(1)).envia(l1);
	}
	
	@Test 
	public void simulacaoDeExceptionNoEncerramentoDeLeiloes()
	{
		Calendar data = Calendar.getInstance();
		data.set(2020,5,10);
		

		Leilao l1 = new CriadorDeLeilao().para("New Car").naData(data).constroi();
		Leilao l2 = new CriadorDeLeilao().para("Playstation 5").naData(data).constroi();	

		List<Leilao> leiloes = Arrays.asList(l1, l2);
		
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		
		when(leilao.correntes()).thenReturn(leiloes);
		doThrow(new RuntimeException()).when(leilao).atualiza(l1);
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		verify(carteiroFake, times(0)).envia(l1);
		verify(leilao, times(1)).atualiza(l2);
		verify(carteiroFake, times(1)).envia(l2);
	}
	
	@Test
	public void simularErroParaTodosOsEnvios()
	{
		Calendar data = Calendar.getInstance();
		data.set(2020,5,10);
		
		Leilao l1 = new CriadorDeLeilao().para("New Car").naData(data).constroi();
		Leilao l2 = new CriadorDeLeilao().para("Playstation 5").naData(data).constroi();	

		List<Leilao> leiloes = Arrays.asList(l1, l2);
		
		EnviaEmailRepository carteiroFake = mock(EnviaEmailRepository.class);
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		
		when(leilao.correntes()).thenReturn(leiloes);
		doThrow(new RuntimeException()).when(leilao).atualiza(any(Leilao.class));
		
		EncerradorDeLeilao encerrador = new EncerradorDeLeilao(leilao, carteiroFake);
		encerrador.encerra();
		
		verify(carteiroFake, never()).envia(any(Leilao.class));
	}
	
	@Test
	public void simularPagamento()
	{
		Calendar antigo = Calendar.getInstance();
		antigo.set(2020,1,10);
		
		Leilao l1 = new CriadorDeLeilao().para("Tv Plasma")
										 .naData(antigo)
										 .lance(new Usuario("Maria"), 250.00)
										 .lance(new Usuario("Alessandro"), 999.00)
										 .constroi();	
		
		List<Leilao> leilaoFake = Arrays.asList(l1);
		
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		PagamentoRepository pagamento = mock(PagamentoRepository.class);	
		Avaliador avaliador = mock(Avaliador.class);
		
		when(leilao.encerrados()).thenReturn(leilaoFake);
		when(avaliador.getMaiorLance()).thenReturn(999.00);
		
		GeradorDePagamento gerador = new GeradorDePagamento(leilao, pagamento, avaliador);
		gerador.gera();
		
		ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
		verify(pagamento).salvar(argumento.capture());
		
		Pagamento pagamentoGerado = argumento.getValue();
		
		assertEquals(999.00, pagamentoGerado.getValor(), 0.00001);
	}
	
	@Test
	public void passaProProximoDiaUtil()
	{
		Calendar antigo = Calendar.getInstance();
		antigo.set(2012, Calendar.APRIL, 7);
		
		Leilao l1 = new CriadorDeLeilao().para("Tv Plasma")
										 .naData(antigo)
										 .lance(new Usuario("Maria"), 250.00)
										 .lance(new Usuario("Alessandro"), 999.00)
										 .constroi();	
		
		LeilaoDaoRepository leilao = mock(LeilaoDaoRepository.class);
		PagamentoRepository pagamento = mock(PagamentoRepository.class);	
		RelogioRepository relogiofake = mock(RelogioRepository.class);

		Avaliador avaliador = mock(Avaliador.class);
		
		when(leilao.encerrados()).thenReturn(Arrays.asList(l1));
		when(avaliador.getMaiorLance()).thenReturn(999.00);
		when(relogiofake.horaSistema()).thenReturn(antigo);
		
		GeradorDePagamento gerador = new GeradorDePagamento(leilao, pagamento, avaliador, relogiofake);
		gerador.gera();
		
		ArgumentCaptor<Pagamento> argumento = ArgumentCaptor.forClass(Pagamento.class);
		verify(pagamento).salvar(argumento.capture());
		
		Pagamento pagamentoGerado = argumento.getValue();
		
		assertEquals(Calendar.MONDAY, pagamentoGerado.getData().get(Calendar.DAY_OF_WEEK));
		assertEquals(9, pagamentoGerado.getData().get(Calendar.DAY_OF_MONTH));
	}
}
